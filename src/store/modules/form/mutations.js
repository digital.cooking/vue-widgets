const mutations = {
  UPDATE_VALUE_A (state, value) {
    state.valueA = value
  },
  UPDATE_VALUE_B (state, value) {
    state.valueB = value
  }
}

export default mutations
