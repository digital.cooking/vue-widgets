import actions from './actions'
import getters from './getters'
import mutations from './mutations'

const defaultState = {
  valueA: null,
  valueB: null
}

const form = {
  namespaced: true,
  state: defaultState,
  getters,
  mutations,
  actions
}

export default form
