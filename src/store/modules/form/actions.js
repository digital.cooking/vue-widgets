const actions = {
  updateValueA (context, value) {
    context.commit('UPDATE_VALUE_A', value)
  },
  updateValueB (context, value) {
    context.commit('UPDATE_VALUE_B', value)
  }
}

export default actions
