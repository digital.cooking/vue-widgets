const getters = {
  getValueA: state => {
    return state.valueA
  },
  getValueB: state => {
    return state.valueB
  }
}

export default getters
