import {
  mapGetters
} from 'vuex'

export default {
  computed: {
    ...mapGetters({
      valueFormA: 'form/getValueA',
      valueFormB: 'form/getValueB'
    })
  }
}
