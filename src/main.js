// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import router from './router'
import store from './store'
import mixins from './mixins'

// import of the widgets components
import FormA from './components/FormA'
import FormB from './components/FormB'

Vue.mixin(mixins)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: {
    FormA,
    FormB
  }
})
